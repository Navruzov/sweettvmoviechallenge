import subprocess
import sys
from os.path import join as pjoin

import setuptools
from setuptools.command.bdist_egg import bdist_egg as _bdist_egg

REQUIREMENTS = [
    'requirements.txt'
]

PACKAGE_NAME = 'sweettvmovie'

DESCRIPTION = 'Library for Kaggle SweetTV Movie Challenge'
URL = 'https://gitlab.com/Navruzov/sweettvmoviechallenge'

# Get global version
# see: https://packaging.python.org/guides/single-sourcing-package-version/
version = {}
with open(pjoin(PACKAGE_NAME, "version.py"), "r") as fh:
    exec(fh.read(), version)
VERSION = version["__version__"]


# class BDistEgg(_bdist_egg):
#     # """Post-installation for installation mode."""
#     def run(self):
#         # install PyTorch on Windows bug
#         print('running post-install...')
#         subprocess.call(
#             [
#                 sys.executable,
#                 '-m', 'pip', 'install',
#                 "torch===1.5.0",
#                 "torchvision===0.6.0",
#                 # "--default-timeout=1000",
#                 "-f",
#                 "https://download.pytorch.org/whl/torch_stable.html"""
#             ]
#         )
#         _bdist_egg.run(self)


def parse_requirements(file):
    lines = (line.strip() for line in open(file))
    return [
        line for line in lines
        if line and not (line.startswith("#") or line.startswith('-'))
    ]


def get_all_requirements():
    requirements = []
    for path in REQUIREMENTS:
        requirements += parse_requirements(path)
    return requirements


setuptools.setup(
    name=PACKAGE_NAME,
    version=VERSION,
    author='F.A.A.',  # TODO: change team name to something meaningful
    author_email='fred.navruzov@gmail.com',
    description=DESCRIPTION,
    url=URL,
    packages=setuptools.find_packages(),
    install_requires=get_all_requirements(),
    # cmdclass={'bdist_egg': BDistEgg},
    license="BSD 3-Clause",
    python_requires='>=3.6',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ]
)
