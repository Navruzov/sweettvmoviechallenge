# sweet.tv - movie recommender package
---
This folder contains source code for utility functions and is an installable package
Clone this repository with
```bash
git clone https://gitlab.com/Navruzov/sweettvmoviechallenge.git
```
cd to repo root
```bash
cd sweettvmoviechallenge
```
and install it with editable mode in mind to avoid reinstalling
```bash
pip install -e .
```
**TODO**: containerization for reproducibility? (after competition ends)

---
Back to the [`top`](../README.md)