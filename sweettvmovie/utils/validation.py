import pandas as pd
import numpy as np


def create_interaction_matrix(df: pd.DataFrame, cat_dict: dict) -> pd.DataFrame:
    """
    Given user-item interaction,
    creates n_user x n_item matrix with
    # of occurrences of (user_id, item_id) as values

    Parameters
    ----------
    df: pd.DataFrame
        Dataframe with interactions

    cat_dict: dict
        Dictionary with computed categorical mappings

    Returns
    -------
    interactions: pd.DataFrame
        Dataframe of (user_id, item_id) shape
    """
    ct = df.groupby(['user_id', 'movie_id']).size().unstack(level=0)
    # add all movies
    ct = ct.reindex(cat_dict['movie_id_to_id'].values()).astype(np.float32)
    # add all users
    ct = ct.T.reindex(cat_dict['user_id_to_id'].values())
    assert ct.shape == (len(cat_dict['user_id_to_id']), len(cat_dict['movie_id_to_id']))

    return ct


def finetune_suggestions(suggestions: pd.DataFrame,
                         user_features: pd.DataFrame,
                         movie_features: pd.DataFrame,
                         historical_views: pd.DataFrame,
                         top_k: int = 5,
                         from_available_only: bool = False,
                         ) -> pd.DataFrame:
    """
    Given `top_k` suggestions, finetune them
    according to some domain if-else rules

    Parameters
    ----------
    suggestions: pd.DataFrame
        Df with `top_k` suggestions
        and `user_id`s as index

    user_features: pd.DataFrame
        User features

    movie_features: pd.DataFrame
        Movie features

    historical_views: pd.DataFrame
        The history of (user_id, movie_id) pairs

    top_k: int, default 5
        How many suggestions to make

    from_available_only: bool, default False
        Whether to select top-n substitutions
        from films with `available_now` flag (if True)
        or from all movies (if False)

    Returns
    -------
    suggestions: pd.DataFrame
        Corrected suggestions df
    """

    # reindex suggestions to all users
    suggestions = suggestions.reindex(user_features.index).fillna(0).values
    suggestions_old = suggestions.copy()

    # stable users --------------------------------------------------------
    stable_users_idx_movies = (
            (user_features.uniqueness_ratio <= 0.33)  # rewatch videos
            & (user_features.serial_share <= 0.33)  # watch mostly movies
            # watch as least 3 time
            & (user_features.total_view_cnt_log >= np.log1p(3.))
            # watched at least 2 unique videos
            & (user_features.unique_movies_watched_log >= np.log1p(1.))
    ).values
    print(
        f'stable user share  (movies), %: '
        f'{stable_users_idx_movies.sum() / len(user_features) * 100:.4f}'
    )

    stable_users_idx_serials = (
            (user_features.uniqueness_ratio <= 0.33)  # rewatch videos
            & (user_features.serial_share >= 0.66)  # watch mostly serials
            # watch as least 3 time
            & (user_features.total_view_cnt_log >= np.log1p(3.))
            # watched at least 2 unique videos
            & (user_features.unique_movies_watched_log >= np.log1p(1.))
    ).values
    print(
        f'stable user share (serials), %: '
        f'{stable_users_idx_serials.sum() / len(user_features) * 100:.4f}'
    )

    # unstable users --------------------------------------------------------------
    unstable_users_idx_movies = (
            (user_features.uniqueness_ratio > 0.66)  # almost always watch smth new
            & (user_features.serial_share <= 0.33)  # watch mostly serials
            # watch as least 3 time
            & (user_features.total_view_cnt_log >= np.log1p(3.))
            # watched at least 2 unique videos
            & (user_features.unique_movies_watched_log >= np.log1p(1.))
    ).values
    print(
        f'unstable user share  (movies), %: '
        f'{unstable_users_idx_movies.sum() / len(user_features) * 100:.4f}'
    )

    unstable_users_idx_serials = (
            (user_features.uniqueness_ratio > 0.8)  # almost always watch something new
            & (user_features.serial_share >= 0.66)  # watch mostly serials
            # watch as least 3 time
            & (user_features.total_view_cnt_log >= np.log1p(3.))
            # watched at least 2 unique videos
            & (user_features.unique_movies_watched_log >= np.log1p(1.))
    ).values
    print(
        f'unstable user share (serials), %: '
        f'{unstable_users_idx_serials.sum() / len(user_features) * 100:.4f}'
    )
    # filter by `available_now` (if needed)
    idx = np.array([True] * len(historical_views)) if not from_available_only else (
        historical_views.movie_id.isin(movie_features[movie_features['available_now']].index)
    )

    # construct top-n serials & movies (per-user and total)
    x = historical_views[idx].groupby(['user_id', 'movie_id']).size(
    ).reset_index().rename(columns={0: 'n_watches'})

    x['is_serial'] = movie_features['is_serial'].astype(bool)
    x['is_serial'].fillna(False, inplace=True)
    x['movie_popularity'] = x.movie_id.map(
        movie_features['unique_users_watches_log'])
    x = x.sort_values(by=['user_id', 'is_serial', 'n_watches', 'movie_popularity'],
                      ascending=[True, False, False, False])

    top_hist_movies_per_user = x[~x.is_serial].groupby(['user_id']).head(top_k)
    top_hist_movies_per_user['rank'] = x[~x.is_serial].groupby(
        ['user_id']).cumcount() + 1

    top_hist_serials_per_user = x[x.is_serial].groupby(['user_id']).head(top_k)
    top_hist_serials_per_user['rank'] = x[x.is_serial].groupby(
        ['user_id']).cumcount() + 1

    # unstack to users x movie_rank (n_users x top_k)
    top_hist_movies_per_user = top_hist_movies_per_user \
        .groupby(['user_id', 'rank'])['movie_id'].first() \
        .unstack(level=[1]).reindex(user_features.index).fillna(0).astype(np.int32)

    top_hist_serials_per_user = top_hist_serials_per_user \
        .groupby(['user_id', 'rank'])['movie_id'].first() \
        .unstack(level=[1]).reindex(user_features.index).fillna(0).astype(np.int32)

    top_hist_serials = movie_features[~movie_features.is_serial.astype(bool)].sort_values(
        by='unique_users_watches_log',
        ascending=False
    ).index[:top_k].values.reshape((1, -1))

    top_hist_movies = movie_features[~movie_features.is_serial.astype(bool)].sort_values(
        by='unique_users_watches_log',
        ascending=False
    ).index[:top_k].values.reshape((1, -1))

    # update top-x preference for users where there is less than top-x
    # by top-x for all
    top_hist_serials_per_user = np.where(
        top_hist_serials_per_user != 0,
        top_hist_serials_per_user,
        top_hist_serials,
    )

    top_hist_movies_per_user = np.where(
        top_hist_movies_per_user != 0,
        top_hist_movies_per_user,
        top_hist_movies,
    )

    # update final recommendations for stable/unstable users
    # pass top serials / top movies for unstable (broadcasting)
    suggestions[unstable_users_idx_movies, :] = top_hist_movies
    suggestions[unstable_users_idx_serials, :] = top_hist_serials

    # pass top past watched ids for stable
    if stable_users_idx_movies.any():
        suggestions[stable_users_idx_movies, :] = \
            top_hist_movies_per_user[stable_users_idx_movies, :]

    if stable_users_idx_serials.any():
        suggestions[stable_users_idx_serials, :] = \
            top_hist_serials_per_user[stable_users_idx_serials, :]

    # these user groups must not intersect
    assert (
                   stable_users_idx_serials
                   & stable_users_idx_movies
                   & unstable_users_idx_serials
                   & unstable_users_idx_movies
           ).sum() == 0

    print(f'suggestions substituted, %: {100 * (suggestions_old != suggestions).mean():.3f}')

    return pd.DataFrame(suggestions, index=user_features.index)
