from typing import List, Union, Dict, Any

import numpy as np
import pandas as pd
from ordered_set import OrderedSet

APK_DTYPE = Union[list, np.ndarray]
MAPK_DTYPE = Union[List[list], List[np.ndarray], np.ndarray]
TRUE_LABELS_DTYPE = Union[List[List[float]], Dict[str, List[float]]]


def apk(actual: APK_DTYPE,
        predicted: APK_DTYPE, k: int = 5) -> np.float:
    """
    Computes the average precision at k.
    This function computes the average prescision at k
    between two lists of items.

    Parameters
    ----------
    actual : list or np.array
        A list of elements that are to be predicted (order doesn't matter)

    predicted : list or np.array
        A list of predicted elements (order does matter)

    k : int, optional
        The maximum number of predicted elements

    Returns
    -------
    score : float
        The average precision at k over the input lists
    """
    if len(predicted) > k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i, p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i + 1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)


def mapk(actual: MAPK_DTYPE, predicted: MAPK_DTYPE, k: int = 5):
    """
    Computes the mean average precision at k.
    This function computes the mean average prescision
    at k between two lists of lists of items.

    Parameters
    ----------
    actual : list or list of lists or list of np.array
             A list of lists of elements that are to be predicted
             (order doesn't matter in the lists)

    predicted : list or list of lists or list of np.array
                A list of lists of predicted elements
                (order matters in the lists)

    k : int, optional
        The maximum number of predicted elements

    Returns
    -------
    score : float
        The mean average precision at k over the input lists
    """
    return np.mean([apk(a, p, k) for a, p in zip(actual, predicted)])


def construct_true_labels(df: pd.DataFrame,
                          sort_col: str = 'ts',
                          sort_ascending: bool = True,
                          user_col: str = 'user_id',
                          item_col: str = 'movie_id',
                          k: int = 5,
                          return_list: bool = True,
                          apply_list_first: bool = True,
                          drop_duplicates: bool = True,
                          ) -> Union[List[List[float]], Dict[Any, List[float]]]:
    """
    Given a dataset, construct a list of first K items (excluding duplicates)
    per user, ordered by `sort_col`. Returns as list of lists or dict of lists
    depending on `return_list` indicator

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame to construct true labels from

    sort_col: str, default 'ts'
        Column to sort in addition to `user_col`

    sort_ascending: bool, default True
        How to sort on sort col (ascending by default)

    user_col: str, default 'user_id'
        Column with user ids

    item_col: str, default 'movie_id'
        Column with item ids

    k : int, optional
        The maximum number of predicted elements

    return_list: bool, default True
        Whether to return results as list of lists of ids
        or as dict of lists of ids with keys = df user index

    apply_list_first: bool, default True
        Whether to take a top-k slice from list of ordered values
        and then - OrderedSet
        or take a top-k of OrderedSet items

    drop_duplicates: bool, default True
        Whether to drop duplicate entries from top-k slice
        [1,1,1,2,2] -> [1,2]

    Returns
    -------
    score : float
        The mean average precision at k over the input lists
    """

    def top_k(x):
        if not drop_duplicates:
            return list(x)[:k]
        else:
            return list(OrderedSet(list(x[:k]))) if apply_list_first else list(OrderedSet(x)[:k])

    labels_true = df.sort_values(by=[user_col, sort_col], ascending=[True, sort_ascending]) \
        .groupby(user_col)[item_col].apply(top_k)

    return labels_true.tolist() if return_list else labels_true.to_dict()
