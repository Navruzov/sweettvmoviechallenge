from os import cpu_count
from typing import List

import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub
from bert.tokenization.bert_tokenization import FullTokenizer
from tensorflow.keras.models import Model
from tqdm.auto import tqdm


def _intersection(list1, list2):
    intersection = len((set(list1).intersection(list2)))
    return float(intersection) / len(set(list2))


def make_movie_features(
        df_views: pd.DataFrame,
        df_movies: pd.DataFrame,
        cat_dict: dict,
        date_col: str = 'ts',
        start_date: str = None,
        end_date: str = None
):
    """
    Make movie features
    over specified time range [start_date, end_date]
    given views dataset, categorical dict and movies info

    Parameters
    ----------
    df_views: pd.DataFrame
        Dataframe with views info

    df_movies: pd.DataFrame
        Dataframe with movies info

    cat_dict: dict
        Dict with categorical encodings

    date_col: str, default `ts`
        The column containing timestamp data in `views` dataframe

    start_date: timestamp
        Date to start from
        If None, no left boundary is applied

    end_date:
        timestamp
        Date to start from
        If None, no left boundary is applied

    Returns
    -------
    movie_features: pd.DataFrame
        Dataframe with built features indexed by `movie_id`
    """
    movie_features = pd.DataFrame(index=df_movies.index)

    df_views[date_col] = pd.to_datetime(df_views[date_col])
    start_date = start_date if start_date else df_views[date_col].min()
    end_date = end_date if end_date else df_views[date_col].max()

    views = df_views[
        (df_views[date_col] >= start_date)
        & (df_views[date_col] <= end_date)
        ].copy()

    col = 'unique_users_watches_log'
    movie_features[col] = (
        views.groupby('movie_id')['user_id'].nunique().astype(np.float32)
    )
    movie_features[col] = np.log1p(
        movie_features[col].fillna(0)
    ).astype(np.float32)

    col = 'unique_users_watches_last_2_month_log'
    movie_features[col] = (
        views[
            views.ts >= views.ts.max() - np.timedelta64(2, 'M')
            ].groupby('movie_id')['user_id'].nunique().astype(np.float32)
    )
    movie_features[col] = np.log1p(
        movie_features[col].fillna(0)
    ).astype(np.float32)

    movie_features['movie_rating_mean'] = np.where(
        df_movies.imdb_rating.notnull(),
        df_movies.imdb_rating,
        np.where(
            df_movies.tmdb_rating.notnull(),
            df_movies.tmdb_rating,
            (df_movies.imdb_rating.mean() + df_movies.tmdb_rating.mean()) / 2
        )
    )

    movie_features['year'] = df_movies.year
    movie_features['is_serial'] = (
            views.groupby(['movie_id'])['episode_id'].max() > 0
    ).astype(np.int32)  # approximation only
    movie_features['is_serial'] = movie_features['is_serial'].fillna(0.)

    # whether the movie has "known" actors in cast
    known_actors = (df_movies.actors.explode().value_counts() >= 5)  # acted in at least 5 films

    col = 'has_known_actors_share'
    movie_features[col] = (
        df_movies.actors.explode().isin(set(known_actors[known_actors].index) - {0})
            .groupby('movie_id').mean()
    ).astype(np.float32)

    movie_features[col].fillna(0., inplace=True)

    # whether the movie has "known" director
    known_directors = (df_movies.director.explode().value_counts() >= 5)  # made at least 5 films

    col = 'has_known_director'
    movie_features[col] = (
        df_movies.director.explode().isin(set(known_directors[known_directors].index) - {0})
            .groupby('movie_id').max()
    ).astype(np.float32)

    movie_features[col].fillna(0., inplace=True)

    movie_genres = df_movies.genres.explode().reset_index().groupby(['movie_id', 'genres']) \
        .size().unstack(level=[1]).fillna(0.).astype(np.float32)
    # norm column sum to 1
    movie_genres = movie_genres / movie_genres.sum(axis=1).values.reshape((-1, 1))
    movie_genres.columns = [f'movie_genre_{i}' for i in range(len(cat_dict['id_to_genres']))]

    movie_features = movie_features.join(movie_genres, how='left')

    movie_features['available_now'] = df_movies['available_now']

    return movie_features


def make_user_features(df_views: pd.DataFrame,
                       df_movies: pd.DataFrame,
                       cat_dict: dict,
                       date_col: str = 'ts',
                       start_date: str = None,
                       end_date: str = None) -> pd.DataFrame:
    """
    Make user features
    over specified time range [start_date, end_date]
    given views dataset, categorical dict and movies info

    Parameters
    ----------
    df_views: pd.DataFrame
        Dataframe with views info

    df_movies: pd.DataFrame
        Dataframe with movies info

    cat_dict: dict
        Dict with categorical encodings

    date_col: str, default `ts`
        The column containing timestamp data in `views` dataframe

    start_date: timestamp
        Date to start from
        If None, no left boundary is applied

    end_date:
        timestamp
        Date to start from
        If None, no left boundary is applied

    Returns
    -------
    user_features: pd.DataFrame
        Dataframe with built features indexed by `user_id`
    """
    df_views[date_col] = pd.to_datetime(df_views[date_col])
    start_date = start_date if start_date else df_views[date_col].min()
    end_date = end_date if end_date else df_views[date_col].max()

    views = df_views[
        (df_views[date_col] >= start_date)
        & (df_views[date_col] <= end_date)
        ].copy()

    views['is_serial'] = (views.episode_id > 0).astype(np.int32)
    views = pd.merge(left=views, right=df_movies, how='left',
                     left_on='movie_id', right_index=True)

    ugroup = views.groupby(['user_id'])

    # user ---------------------------------------------------------
    user_features = pd.DataFrame(index=pd.Index(
        name='user_id', data=sorted(views.user_id.unique())))
    user_features['total_view_cnt_log'] = np.log1p(ugroup.size())
    user_features['view_cnt_monthly_avg_log'] = np.log1p(
        views.groupby(['user_id', 'view_year', 'view_month']
                      ).size().groupby(level=[0]).mean()
    )
    user_features['uniqueness_ratio'] = ugroup['movie_id'].nunique() / \
                                        ugroup.size()
    user_features['unique_movies_watched_log'] = np.log1p(
        ugroup['movie_id'].nunique())

    user_features['time_between_views_days_avg'] = (
            views.sort_values(['user_id', 'ts'])
            .groupby('user_id').ts.diff().fillna(pd.Timedelta(seconds=0))
            .dt.total_seconds().groupby(views.user_id).mean() / 86400
    )

    # whether user has favorite movie_id
    # (favorite ~ top movie_id counts more than 1/3 of all views)
    user_features['has_favorite_movie'] = (
            views.groupby('user_id')['movie_id'].value_counts(normalize=True)
            .groupby('user_id').first() > 0.33
    )

    # movie ---------------------------------------------------------
    user_features['serial_share'] = ugroup['is_serial'].mean()
    user_features['user_film_rating_avg'] = (
                                                    ugroup['imdb_rating'].mean() + ugroup['tmdb_rating'].mean()) / 2
    user_features['user_film_rating_avg'].fillna(
        user_features['user_film_rating_avg'].mean(), inplace=True
    )
    user_features['user_film_rating_std'] = (
                                                    ugroup['imdb_rating'].std() + ugroup['tmdb_rating'].std()) / 2
    user_features['user_film_rating_std'].fillna(
        user_features['user_film_rating_std'].mean(), inplace=True
    )
    user_features['user_film_year_avg'] = ugroup['year'].mean()

    # user genres preferences ----------------------------------------
    grouping = ['user_id', 'genres']
    genres_ct = views.set_index('user_id')['genres'].explode().reset_index() \
        .groupby(grouping).size().reindex(
        pd.MultiIndex.from_product(
            iterables=(views.user_id.unique(), cat_dict['id_to_genres']),
            names=grouping
        )
    ).fillna(0).unstack(level=[1]).astype(np.float32)

    # normalize row to 1 (genre shares among single user)
    genres_ct = genres_ct / genres_ct.sum(axis=1).values.reshape((-1, 1))

    try:
        genres_ct.columns = [
            f'user_genre_share__{cat_dict["genres_to_id"][c]}' for c in genres_ct.columns
        ]
    except KeyError:
        genres_ct.columns = [
            f'user_genre_share__{c}' for c in genres_ct.columns
        ]

    # month movies share in month+1
    gr_idx = ['user_id', 'view_year', 'view_month']
    t = views.groupby(gr_idx)['movie_id'].apply(
        frozenset).reset_index().rename(columns={'movie_id': 'movie_set'})

    t['movie_set_prev_month'] = t.groupby(
        'user_id')['movie_set'].shift().values
    null_idx = t['movie_set_prev_month'].isnull()
    t.loc[null_idx, 'movie_set_prev_month'] = [
                                                  frozenset([-1])] * null_idx.sum()

    t['int_sim'] = t.apply(
        lambda x: _intersection(
            x['movie_set'], x['movie_set_prev_month']
        ),
        axis=1
    )

    user_features['prev_month_intersection_share_mean'] = \
        t.groupby('user_id')['int_sim'].mean()

    user_features = user_features.merge(
        genres_ct, left_index=True, right_index=True, how='left')

    return user_features


def build_bert_embedder(max_seq_length: int = 256):
    """
    Builds a model over pretrained BERT
    that returns pooled output

    Parameters
    ----------
    max_seq_length: int
        Sequence length (max)

    Returns
    -------
    embedder
    """
    blayer_url = "https://tfhub.dev/tensorflow/bert_en_uncased_L-12_H-768_A-12/1"

    input_word_ids = tf.keras.layers.Input(shape=(max_seq_length,), dtype=tf.int32,
                                           name="input_word_ids"
                                           )
    input_mask = tf.keras.layers.Input(shape=(max_seq_length,), dtype=tf.int32,
                                       name="input_mask"
                                       )
    segment_ids = tf.keras.layers.Input(shape=(max_seq_length,), dtype=tf.int32,
                                        name="segment_ids"
                                        )
    bert_layer = hub.KerasLayer(
        blayer_url,
        trainable=False
    )

    pooled_output, _ = bert_layer(
        [input_word_ids, input_mask, segment_ids]
    )

    embedder = Model(
        inputs={
            'input_word_ids': input_word_ids,
            'input_mask': input_mask,
            'segment_ids': segment_ids
        },
        outputs=[pooled_output]
    )

    embedder.build(input_shape=(None, max_seq_length))

    return embedder


def get_bert_embeddings(texts: List[str], embedder, max_seq_length: int = 256) -> np.ndarray:
    """
    Get bert pooled embeddings

    Parameters
    ----------
    texts : list of str
        Texts to get embeddings for

    embedder: bert_embedder
        BERT embedder (pretrained English uncased BERT)

    max_seq_length: int, default 256
        Max seq length to be encoded

    Returns
    -------
    bert_embeddings: np.array
        2d array of shape (num_samples, emb_size=768)
    """

    def get_masks(tokens, max_seq_length):
        """Mask for padding"""
        if len(tokens) > max_seq_length:
            raise IndexError("Token length more than max seq length!")
        return [1] * len(tokens) + [0] * (max_seq_length - len(tokens))

    def get_segments(tokens, max_seq_length):
        """Segments: 0 for the first sequence, 1 for the second"""
        if len(tokens) > max_seq_length:
            raise IndexError("Token length more than max seq length!")
        segments = []
        current_segment_id = 0
        for token in tokens:
            segments.append(current_segment_id)
            if token == "[SEP]":
                current_segment_id = 1
        return segments + [0] * (max_seq_length - len(tokens))

    def get_ids(tokens, tokenizer, max_seq_length):
        """Token ids from Tokenizer vocab"""
        token_ids = tokenizer.convert_tokens_to_ids(tokens)
        input_ids = token_ids + [0] * (max_seq_length - len(token_ids))
        return input_ids

    def prepare_bert_input(texts, tokenizer, max_seq_length):
        """Prepare BERT input"""
        ids, masks, segments = [], [], []
        for text in tqdm(texts):
            tokens = tokenizer.tokenize(text=text)[:max_seq_length]
            i = get_ids(tokens, tokenizer=tokenizer, max_seq_length=max_seq_length)
            mask = get_masks(tokens, max_seq_length=max_seq_length)
            segment = get_segments(tokens, max_seq_length=max_seq_length)
            ids.append(i)
            masks.append(mask)
            segments.append(segment)

        return ids, masks, segments

    vocab_file = embedder.layers[-1].resolved_object.vocab_file.asset_path.numpy()
    do_lower_case = embedder.layers[-1].resolved_object.do_lower_case.numpy()
    tokenizer = FullTokenizer(vocab_file, do_lower_case)

    input_ids, input_masks, input_segments = prepare_bert_input(
        texts=texts,
        tokenizer=tokenizer,
        max_seq_length=max_seq_length,
    )

    return embedder.predict(
        x=[
            np.array(input_ids),
            np.array(input_masks),
            np.array(input_segments)
        ],
        batch_size=None,
        workers=max(1, cpu_count() // 4),
    )


def get_use_embedder():
    """
    Get TFHub USE embedder:
    "https://tfhub.dev/google/universal-sentence-encoder-large/5"

    Returns
    -------
    embedder
        USE embedder (large, size=512)
    """
    embedder = hub.load("https://tfhub.dev/google/universal-sentence-encoder-large/5")
    return embedder


def get_use_embeddings(texts: List[str], embedder) -> pd.DataFrame:
    """
    Get USE embeddings for texts,
    given pretrained USE embedder

    Parameters
    ----------
    texts: list of str
        Texts to encode

    embedder
        Pretrained USE embedder

    Returns
    -------
    use_embeddings: pd.DataFrame
    """
    use_embeddings = pd.DataFrame(
        data=embedder(texts).numpy(),
        columns=[f'use_emb__{i}' for i in range(512)]
    )

    return use_embeddings
