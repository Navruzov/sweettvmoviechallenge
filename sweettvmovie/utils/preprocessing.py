import pickle
import logging
import sys
from typing import Dict, NoReturn, Tuple, Union, List

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                    level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

from collections import defaultdict
from os.path import join as pjoin

import numpy as np
import pandas as pd

pd.options.display.max_rows = 100
pd.options.display.max_colwidth = 200

INPUT_DATA_DIR = pjoin('..', 'data', 'raw')
OUTPUT_DATA_DIR = pjoin('..', 'data', 'processed')
UNK_ID = 0
UNK_VAL = '<unk>'
NONAME_VAL = '<noname>'


def process_data(input_data_dir: str = INPUT_DATA_DIR,
                 output_data_dir: str = OUTPUT_DATA_DIR,
                 drop_unknown_movies: bool = True,
                 filter_infrequent: int = 0,
                 filter_type: str = 'users',
                 return_data: bool = False) -> Union[NoReturn, Tuple[Union[pd.DataFrame, dict]]]:
    """
    Given initial data sources, apply preprocessing,
    compute categorical encodings
    and store it in optimized format

    Parameters
    ----------
    input_data_dir: str
        The path to raw data

    output_data_dir: str
        The path to store processed data

    drop_unknown_movies: bool, default True
        Whether to drop rows from `views` dataset
        if `movie_id` is not found in `movies.movie_id`

    filter_infrequent: int, default 0
        Low boundary of a filter (if 0 - does nothing)
        if N - drops from movie DB and view history movies
        that has been watched less than N times
        (if `filter_type` == `views`)
        or less than by N unique users
        (if `filter_type` == `users`)

    filter_type: str, default 'users'
        How to filter infrequent
        (by unique users cnt or by total views)

    return_data: bool, default False

    Returns
    -------
    result: None or Tuple
        If `return_data` is True, returns a tuple of
        (views, movies, submission, cat_dict).
        Returns `None` otherwise
    """

    def compute_cat_dict() -> Dict[str, dict]:
        """
        Encode categorical columns
        to reduced integer representation

        Returns
        -------
        result: dict
            Dict with stored direct and reverse mappings
            (`{feature}_to_id` and `id_to_{feature}`)
        """
        logger.info('Encode categorical features...')
        cat_dict = defaultdict(dict)
        # users
        cat_dict['user_id_to_id'] = {int(u): i + 1 for i, u in enumerate(sorted(users_train | users_test))}
        # assume no unknown users are found
        # cat_dict['user_id_to_id'][UNK_VAL] = 0
        cat_dict['id_to_user_id'] = {v: k for (k, v) in cat_dict['user_id_to_id'].items()}
        logger.info(f"# users: {len(cat_dict['user_id_to_id'])}")

        # genres
        cat_dict['genres_to_id'] = {g: i + 1 for i, g
                                    in enumerate(sorted(set(genres.index) | set(genres_train.index) - {UNK_VAL}))}
        cat_dict['genres_to_id'][UNK_VAL] = 0
        cat_dict['id_to_genres'] = {v: k for (k, v) in cat_dict['genres_to_id'].items()}
        logger.info(f"# genres: {len(cat_dict['genres_to_id'])}")

        # directors
        directors = movie_db.director.str.lower().str.strip().str.split(',').explode().fillna(UNK_VAL).value_counts()
        # replace 1film directors with <noname> token
        # directors.index = np.where(directors > 1, directors.index, '<noname>')

        cat_dict['director_to_id'] = {d: i + 1 for i, d in enumerate(sorted(set(directors.index) - {UNK_VAL}))}
        cat_dict['director_to_id'][UNK_VAL] = 0
        cat_dict['id_to_director'] = {v: k for (k, v) in cat_dict['director_to_id'].items()}
        logger.info(f"# directors: {len(cat_dict['director_to_id'])}")

        # actors
        actors = movie_db.actors.str.lower().str.strip().str.split(',').explode().fillna(UNK_VAL).value_counts()
        # replace 1film actors with <noname> token
        # actors.index = np.where(actors > 1, actors.index, '<noname>')
        cat_dict['actors_to_id'] = {a: i + 1 for i, a in enumerate(sorted(set(actors.index) - {UNK_VAL}))}
        cat_dict['actors_to_id'][UNK_VAL] = 0
        cat_dict['id_to_actors'] = {v: k for (k, v) in cat_dict['actors_to_id'].items()}
        logger.info(f"# actors: {len(cat_dict['actors_to_id'])}")

        # movie ids (dense pack)
        movie_ids = sorted(set(views_db.movie_id) | set(movie_db.index))
        cat_dict['movie_id_to_id'] = {m: i + 1 for i, m in enumerate(movie_ids)}
        cat_dict['movie_id_to_id'][UNK_VAL] = 0
        cat_dict['id_to_movie_id'] = {v: k for (k, v) in cat_dict['movie_id_to_id'].items()}
        logger.info(f"# movies: {len(cat_dict['movie_id_to_id'])}")

        return cat_dict

    # submission file #######################################################
    logger.info('Working with `submission` file...')
    submission = pd.read_csv(pjoin(input_data_dir, 'submission.csv'))
    logger.info(f'df shape: {submission.shape}')

    users_test = set(submission.user_id)
    logger.info(f'Users found in test: {len(users_test)}')
    assert len(submission) == len(users_test), 'duplicate user_ids were found in `submission`'

    # views file ############################################################
    logger.info('Working with `views` file...')
    views_db = pd.read_csv(
        pjoin(input_data_dir, 'movies_dataset_10 months.csv'),
        parse_dates=['ts']
    )
    views_db['view_month'] = views_db['ts'].dt.month.astype(np.uint8)
    views_db['view_year'] = views_db['ts'].dt.year.astype(np.uint16)
    views_db['view_weekday'] = views_db['ts'].dt.weekday.astype(np.uint8)
    views_db['view_monthday'] = views_db['ts'].dt.day.astype(np.uint8)
    logger.info(f'df shape: {views_db.shape}')

    users_train = set(views_db.user_id)
    logger.info(f'Users found in train: {len(users_train)}')
    logger.info(
        f'Test users found in train: '
        f'{len(users_test & users_train)}/{len(users_test)}'
    )
    # define if movie is 'episodic'
    episodic = views_db.groupby('movie_id')['episode_id'].max() > 0
    logger.info(f'episodic movies views share, %: {episodic.mean() * 100:.4f}')

    # movie db file #########################################################
    movie_db = pd.read_csv(pjoin(input_data_dir, 'movies.csv')) \
        .rename(columns={'id': 'movie_id'}).set_index('movie_id')
    assert len(movie_db) == movie_db.index.nunique()
    logger.info(f'Movie db shape: {movie_db.shape}')
    logger.info(f"NaN count:\n{movie_db.replace('', np.nan).isnull().sum() / len(movie_db)}")
    logger.info(
        f'train intersection with movie db: '
        f'{len(set(movie_db.index) & set(views_db.movie_id))}/{views_db.movie_id.nunique()}'
    )

    if drop_unknown_movies:
        logging.info('Dropping unknown `movie_id`s from `views`')
        logger.info(f'View db shape before deletion: {views_db.shape}')
        views_db = views_db[views_db.movie_id.isin(movie_db.index)]
        logger.info(f'View db shape  after deletion: {views_db.shape}')

    if filter_infrequent > 0:  # drop rare movies from DB and views
        logger.info(f'filtering out infrequent movies (`{filter_type}` <= {filter_infrequent})')
        gr = views_db.groupby(['movie_id'])
        if filter_type == 'users':
            rare_movies = (gr['user_id'].nunique() <= filter_infrequent)
        elif filter_type == 'views':
            rare_movies = (gr.size() <= filter_infrequent)
        else:
            raise NotImplementedError(f'unknown filter type: `{filter_type}`')
        rare_movies = rare_movies[rare_movies].index
        logger.info(f'(BEFORE) movies DB shape: {movie_db.shape} views DB shape: {views_db.shape}')
        # filter movie db
        movie_db = movie_db[~movie_db.index.isin(rare_movies)].copy()
        # filter views
        views_db = views_db[~views_db.movie_id.isin(rare_movies)].copy()
        logger.info(f'(AFTER)  movies DB shape: {movie_db.shape} views DB shape: {views_db.shape}')

    movie_db['available_now'] = movie_db['available_now'].map({'yes': True, 'no': False}).astype(bool)
    movie_db['imdb_rating'] = movie_db['imdb_rating'].astype(np.float32)
    movie_db['tmdb_rating'] = movie_db['tmdb_rating'].astype(np.float32)
    movie_db['year'] = movie_db['year'].astype(np.int16)

    # extract genres
    genres = movie_db.genres.str.lower().str.strip().str.split(',').explode() \
        .value_counts(ascending=False, normalize=True)

    genres_train = (
        views_db.movie_id.map(movie_db.genres).replace('', np.nan)
            .fillna(UNK_VAL).astype('str')
            .str.lower().str.strip().str.split(',')
            .explode().value_counts(ascending=False, normalize=True)
    )
    logger.info(f'top-5 genres in DB:\n{genres[:5]}')
    logger.info(f'top-5 genres in views:\n{genres_train[:5]}')

    # drop useless columns
    cols_to_drop = [
        'producer', 'art', 'writers', 'music',
    ]
    print(f'shape before drop: {movie_db.shape}')
    movie_db = movie_db.drop(cols_to_drop, axis=1)
    print(f'shape after  drop: {movie_db.shape}')

    # encode categorical columns
    logger.info('encoding categoricals...')
    category_dict = compute_cat_dict()

    # replace categoricals with int encoded versions
    logger.info('replacing categoricals...')
    submission['user_id'] = submission['user_id'].map(category_dict['user_id_to_id'])
    views_db['user_id'] = views_db['user_id'].map(category_dict['user_id_to_id'])
    movie_db['genres'] = movie_db['genres'].replace(np.nan, UNK_VAL) \
        .str.lower().str.strip().str.split(',').apply(
        lambda x: [category_dict['genres_to_id'][i] for i in x] if x else [UNK_ID]
    )
    movie_db['actors'] = movie_db['actors'].replace(np.nan, UNK_VAL) \
        .str.lower().str.strip().str.split(',').apply(
        lambda x: [category_dict['actors_to_id'][i] for i in x] if x else [UNK_ID]
    )

    movie_db['director'] = movie_db['director'].replace(np.nan, UNK_VAL) \
        .str.lower().str.strip().str.split(',').apply(
        lambda x: [category_dict['director_to_id'][i] for i in x] if x else [UNK_ID]
    )

    # substitute movie ids
    movie_db.index = movie_db.index.map(category_dict['movie_id_to_id']) \
        .fillna(category_dict['movie_id_to_id'][UNK_VAL])
    views_db.movie_id = views_db.movie_id.map(category_dict['movie_id_to_id']) \
        .fillna(category_dict['movie_id_to_id'][UNK_VAL])

    # save preprocessed files ################################################
    logger.info('saving preprocessed files...')
    movie_db.to_pickle(pjoin(output_data_dir, 'movies.pkl'))
    views_db.to_pickle(pjoin(output_data_dir, 'views.pkl'))
    submission.to_pickle(pjoin(output_data_dir, 'submission.pkl'))
    # save cat dict
    with open(pjoin(output_data_dir, 'cat_dict.pkl'), 'wb') as fp:
        pickle.dump(category_dict, fp)

    if return_data:
        return load_processed_data(processed_data_dir=output_data_dir)


def encode_categorical(df: pd.DataFrame, cat_dict: Dict[str, dict],
                       ignore_columns: List[str] = None) -> pd.DataFrame:
    """
    Given category dict and dataframe, encodes columns
    to reduced int codes

    Parameters
    ----------
    df: pd.DataFrame
        Df to reverse encode categoricals

    cat_dict: dict
        Dictionary with computed categorical mappings

    ignore_columns: list, default None
        Whether to ignore some columns
        even if they are found in `cat_dict`

    Returns
    -------
    result: pd.DataFrame
        Reverse encoded dataframe (a copy)
    """
    ignore_columns = [] if not ignore_columns else ignore_columns
    df_cp = df.copy(deep=True)
    idx_name = df_cp.index.name
    df_cp = df_cp.reset_index(drop=False if idx_name else True)
    for c in df_cp.columns:
        if f'{c}_to_id' in cat_dict and c not in ignore_columns:
            try:
                df_cp[c] = df_cp[c].map(cat_dict[f'{c}_to_id'])
            except (TypeError, ValueError):
                df_cp[c] = df_cp[c].apply(lambda x: [cat_dict[f'{c}_to_id'][i] for i in x] if x else [UNK_ID])
    return df_cp.set_index(idx_name) if idx_name else df_cp


def inverse_encode_categorical(df: pd.DataFrame, cat_dict: Dict[str, dict],
                               ignore_columns: List[str] = None) -> pd.DataFrame:
    """
    Given category dict and dataframe, encodes columns
    back to initial format (if any)

    Parameters
    ----------
    df: pd.DataFrame
        Df to reverse encode categoricals

    cat_dict: dict
        Dictionary with computed categorical mappings

    ignore_columns: list, default None
        Whether to ignore some columns
        even if they are found in `cat_dict`

    Returns
    -------
    result: pd.DataFrame
        Reverse encoded dataframe
    """
    ignore_columns = [] if not ignore_columns else ignore_columns
    df_cp = df.copy(deep=True)
    idx_name = df_cp.index.name
    df_cp = df_cp.reset_index(drop=False if idx_name else True)
    for c in df_cp.columns:
        if f'{c}_to_id' in cat_dict and c not in ignore_columns:
            try:
                df_cp[c] = df_cp[c].map(cat_dict[f'id_to_{c}'])
            except (TypeError, ValueError):
                df_cp[c] = df_cp[c].apply(lambda x: [cat_dict[f'id_to_{c}'][i] for i in x] if x else [UNK_ID])
    return df_cp.set_index(idx_name) if idx_name else df_cp


def load_processed_data(processed_data_dir: str = OUTPUT_DATA_DIR) -> Tuple:
    """
    Load preprocessed files back

    Parameters
    ----------
    processed_data_dir: str
        The path to processed files

    Returns
    -------
    results: tuple
        A tuple of (views, movies, submission, cat_dict)
        of type (pd.DataFrame, pd.DataFrame, pd.DataFrame, dict)

    """
    with open(pjoin(processed_data_dir, 'cat_dict.pkl'), 'rb') as fp:
        cat_dict = pickle.load(fp)

    views = pd.read_pickle(pjoin(processed_data_dir, 'views.pkl'))
    movies = pd.read_pickle(pjoin(processed_data_dir, 'movies.pkl'))
    submission = pd.read_pickle(pjoin(processed_data_dir, 'submission.pkl'))

    return views, movies, submission, cat_dict
