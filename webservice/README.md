<h1>This is a DD model service</h1>


<h3> Build </h3>

```
docker build -t model_service:latest .
```

<h3> Deploy </h3>

 ```
docker run -d -p <PORT>:3143 model_service:latest
 ```
<PORT> defines port on which one you want to run your application.
 
 For example:

 ```
docker run -d -p 3143:3143 model_service:latest
 ```

You can check that this service is running if you get 'pong' response by requesting ```http://<HOST>:<PORT>/```.
    
<h3> Request format </h3>
For single user recommendation with history of interactions : 1, 2, 3, 4, 5, 6
    
 ```
http://<HOST>:<PORT>/api/recommend?history=1,2,3,4,5,6
 ```
    
For multiple users recommendation: user1:1,2,3,4,5,6  user2:12,12304,123,412
 ```
http://<HOST>:<PORT>/api/recommend?history=1,2,3,4,5,6;12,12304,123,412
 ```    
