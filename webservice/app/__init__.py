from flask import Flask
from app.lstm_model import RecommenderLSTM

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

    except RuntimeError as e:
        print(e)


app = Flask(__name__)

recommender_lstm = RecommenderLSTM()
