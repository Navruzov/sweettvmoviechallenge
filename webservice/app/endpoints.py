import ast

from flask import request, jsonify
from app import app, recommender_lstm


@app.route('/api/recommend', methods=['GET'])
def recomendation_pipeline():
    try:
        sequences = str(request.args.get('history'))
        sequences = sequences.split(';')
        converted_sequences = [[int(movie) for movie in sequence.split(',')] for sequence in sequences] 
        resp = recommender_lstm.predict(converted_sequences)
        return jsonify({'recommendations': resp})
    except Exception as e:
        return str(e)

@app.route('/', methods=['GET', 'POST'])
def ping():
    return 'pong'
