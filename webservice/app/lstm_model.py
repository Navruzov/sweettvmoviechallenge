import pickle
import numpy as np
from keras.models import model_from_json


class RecommenderLSTM:
    def __init__(self, path='app/model_data'):
        self.input_seq_maxlen = 100
        #load id converters
        self.cat_to_element_uid_inp = pickle.load(open(path + '/cat_to_element_uid_inp.pkl', 'rb'))
        self.element_uid_to_cat_inp = pickle.load(open(path + '/element_uid_to_cat_inp.pkl', 'rb'))
        
        #load lstm model
        json_file = open(f'{path}/lstm_agg.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.lstm_model = model_from_json(loaded_model_json)
        # load weights into new model
        self.lstm_model.load_weights(f'{path}/lstm_agg.h5')
        
    def predict(self, sequences):
        if not isinstance(sequences[0], list):
            sequences = [sequences]
        converted_sequences = []
        for seq in sequences:
            converted_sequences.append(
                (self.input_seq_maxlen - len(seq[:self.input_seq_maxlen]))*[0] + \
                [self.element_uid_to_cat_inp.get(movie, 0) for movie in seq[-self.input_seq_maxlen:]]
            )
        predicts = self.lstm_model.predict(np.array(converted_sequences))
        predicts = np.argsort(-predicts)[:, :5]
        converted_predicts = []
        for single_user_pred in predicts:
            converted_predicts.append([self.cat_to_element_uid_inp.get(movie, 12304) for movie in single_user_pred])
        return converted_predicts[0] if len(converted_predicts) <= 1 else converted_predicts
        