from waitress import serve

from app.endpoints import app


if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=3142, expose_tracebacks=True)
