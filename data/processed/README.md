# Competition data: Pre-Processed
---

This folder holds transformed, cleaned and preprocessed data

---
Keep in mind, this folder's content isn't tracked in VCS

---
Back to the [`top`](./../../README.md)