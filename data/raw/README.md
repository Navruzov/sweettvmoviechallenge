# Competition data: Raw
---

This folder holds initial, unprocessed data sources

---
Keep in mind, this folder's content isn't tracked in VCS

---
Back to the [`top`](./../../README.md)