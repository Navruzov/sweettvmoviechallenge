# Competition data
---
- [`raw`](./raw) dir holds initial, unprocessed data sources
- [`processed`](./processed) dir holds transformed, cleaned and preprocessed stuff
---
Keep in mind, this folder's content isn't tracked in VCS

---
Back to the [`top`](./../README.md)