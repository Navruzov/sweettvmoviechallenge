# sweet.tv - movie recommender repo
---
This is the codebase for Kaggle's 
[sweet.tv - movie recommender](https://www.kaggle.com/c/sweettv-movie-recommender) competition

The project is organized as follows:
- [`data`](./data) folder contains both [`raw`](./data/raw) and [`processed`](./data/raw) data folders 
<br>and isn't tracked in VCS by default
- [`sweettvmovie`](./sweettvmovie) folder holds the package itself, 
<br>see it for the installation guide
- [`notebooks`](./notebooks) folder contains `.ipynb` notebooks with reference details
<br>as well as experimenting

---
**Requirements**: 
- Python >= 3.6, 
- [other requirements](./requirements.txt)
